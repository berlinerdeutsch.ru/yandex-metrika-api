# Пакет для работы с API Яндекс Метрика
## Пример отправки CRM заказов
```python
from yandex_metrika_api.client import YandexMetrikaApi
import logging

logging.basicConfig(
    format="%(asctime)s:%(levelname)s:%(message)s",
    datefmt="%m/%d/%Y %I:%M:%S %p",
    level=logging.DEBUG,
)

counter = '00000000000000'
token = 'XXXXXXXXXXXXXXXXXXXXXXXXX'
metrica_client = YandexMetrikaApi(counter, token)

file_path = '/Users/sammy/crm_orders.csv'
metrica_client.send_crm_simple_orders(file_path)
```