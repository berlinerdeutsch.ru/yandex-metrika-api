import logging
from typing import Any, Dict

import requests


class YandexMetrikaApiException(Exception):
    def __init__(self, message: str = None) -> None:
        """
        Exception for Yandex Metrika API errors.

        Args:
            message (str, optional): Error message. Defaults to None.
        """
        self.message = message
        super().__init__(message)


class YandexMetrikaApi:
    def __init__(
        self,
        counter: str,
        api_token: str,
        base_url: str = "https://api-metrika.yandex.net",
    ) -> None:
        """
        Initializes the YandexMetrikaApi instance

        Args:
            counter (str): Yandex Metrika counter ID.
            api_token (str): Yandex Metrika API token.
            base_url (str, optional): Base URL for the Yandex Metrika API. Defaults to "https://api-metrika.yandex.net".
        """
        self.base_url = base_url
        self.counter = counter
        self.api_token = api_token

    def _make_request(
        self,
        method: str,
        path: str,
        data: Dict[str, Any] | None = None,
        params: Dict[str, Any] | None = None,
        files: Dict[str, Any] | None = None,
    ) -> Dict[str, Any] | str:
        """
        Make a request to the Yandex Metrika API.

        Args:
            method (str): HTTP method (e.g., "GET", "POST").
            path (str): API endpoint path.
            data (Dict[str, Any], optional): JSON data for the request body. Defaults to None.
            params (Dict[str, Any], optional): Query parameters. Defaults to None.
            files (Dict[str, Any], optional): Files to be sent with the request. Defaults to None.

        Returns:
            Dict[str, Any] | str: JSON response data or raw text.

        Raises:
            YandexMetrikaApiException: If there is an error while making the request to the Yandex Metrika API.
        """
        url = f"{self.base_url}/{path}"

        headers = {"Authorization": f"OAuth {self.api_token}"}

        logging.debug(
            f"Sending {method} request to {url} with headers: {headers}; query params: {params}; data: {data}; files: {files}"
        )
        try:
            r = requests.request(
                method, url, headers=headers, json=data, params=params, files=files
            )
            r.raise_for_status()
        except requests.TooManyRedirects as err:
            raise YandexMetrikaApiException(f"Too many redirects: {err}")
        except requests.HTTPError as err:
            raise YandexMetrikaApiException(f"HTTPError occurred: {err}")
        except requests.Timeout as err:
            raise YandexMetrikaApiException(f"Timeout error: {err}")
        except requests.ConnectionError as err:
            raise YandexMetrikaApiException(
                f"Connection is lost, try again later: {err}"
            )
        except requests.exceptions.RequestException as err:
            raise YandexMetrikaApiException(f"Some error occurred: {err}")

        logging.debug(f"Response: {r.status_code}, {r.content}")

        try:
            response_data = r.json()
        except requests.JSONDecodeError:
            response_data = r.text

        return response_data

    def send_crm_simple_orders(
        self, file_path: str, merge_mode: str = "SAVE", delimeter_type: str = ","
    ) -> Dict[str, Any]:
        """
        Send simple orders data to Yandex Metrika CDP.

        Args:
            file_path (str): Path to the CSV file containing simple orders data.
            merge_mode (str, optional): Merge mode for data processing. Defaults to "SAVE".
            delimeter_type (str, optional): Delimiter type for CSV file. Defaults to ",".

        Returns:
            Dict[str, Any]: Response data from the Yandex Metrika API.

        Raises:
            YandexMetrikaApiException: If there is an error while making the request to the Yandex Metrika API.

        Notes:
            https://yandex.ru/dev/metrika/doc/api2/practice/crm/simple-orders-data.html
        """
        path = f"cdp/api/v1/counter/{self.counter}/data/simple_orders"

        params = {"merge_mode": merge_mode, "delimeter_type": delimeter_type}
        file = open(file_path, "r").read()
        files = {"file": file}

        return self._make_request("POST", path, params=params, files=files)
